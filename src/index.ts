import * as signalR from "@aspnet/signalr";

const outputElement = document.getElementById("output");
const messageForm = document.getElementById("messageForm") as HTMLFormElement;
const inputMessage = document.getElementById(
  "inputMessage"
) as HTMLInputElement;
const localuser = new Date().getTime().toString();

const connection = new signalR.HubConnectionBuilder().withUrl("/hub").build();

connection.on("messageReceived", (username: string, message: string) => {
  const quote = document.createElement("blockquote");
  if (username === localuser) {
    quote.classList.add("is-me");
  }

  quote.innerHTML = `<header><cite>${username}</cite></header><p>${message}</p>`;
  outputElement.appendChild(quote);
  quote.scrollIntoView({ behavior: "smooth" });
});

connection.start().catch(console.error);

messageForm.addEventListener("submit", event => {
  event.preventDefault();
  send();
});

async function send() {
  connection
    .send("newMessage", localuser, inputMessage.value)
    .then(() => (inputMessage.value = ""));
}
