# SignalR POC

Super-basic message platform demoing SignalR (based on [this tutorial](https://docs.microsoft.com/en-us/aspnet/core/tutorials/signalr-typescript-webpack?view=aspnetcore-3.0&tabs=visual-studio-code)).

## Prerequisites

- Node 10+
- Yarn
- [.NET Core 3](https://dotnet.microsoft.com/download/dotnet-core/3.0)

## Getting Started

Open a terminal at the project root and run:

```bash
yarn start
```

Open a browser at http://localhost:5000 - open another browser (or private window) to the same URL.

Send messages!
